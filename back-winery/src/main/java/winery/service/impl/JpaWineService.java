package winery.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import winery.model.Wine;
import winery.repository.WineRepository;
import winery.service.WineService;

@Service
public class JpaWineService implements WineService {

	@Autowired
	WineRepository wineRepository;
	
	@Override
	public Optional<Wine> getOne(Long id) {
		
		return wineRepository.getWineById(id);
	}

	@Override
	public Wine deleteById(Long id) {
		
		Optional<Wine> wineOptional = wineRepository.findById(id);
		
		if(wineOptional.isPresent()) {
			
			Wine wine = wineOptional.get();
			wineRepository.delete(wine);
			return wine;
		}
		
		
		return null;
	}

	@Override
	public Page<Wine> getAll(Long wineryId, String wineName, int pageNum) {
		
		if(wineName == null) {
			wineName = "";
		}
		
		if(wineryId == null) {
			return wineRepository.findAllByWineNameContainingIgnoreCase(wineName, PageRequest.of(pageNum, 5));
		}
		
		return wineRepository.findAllByWineryIdAndWineNameContainingIgnoreCase(wineryId, wineName, PageRequest.of(pageNum, 4));
		
	}

	@Override
	public Wine create(Wine wine) {
		
		return wineRepository.save(wine);
	}

	@Override
	public Wine update(Wine wine) {
		
		return wineRepository.save(wine);
	}

}
