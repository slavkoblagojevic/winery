package winery.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import winery.model.Winery;
import winery.repository.WineryRepository;
import winery.service.WineryService;

@Service
public class JpaWineryService implements WineryService {

	@Autowired
	WineryRepository wineryRepository;
	
	@Override
	public List<Winery> getAll() {
		
		return wineryRepository.findAll();
	}

	@Override
	public Winery getOne(Long id) {
		
		return wineryRepository.getOne(id);
	}

}
