package winery.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import winery.model.TypeOfWine;
import winery.repository.TypeOfWineRepository;
import winery.service.TypeOfWineService;

@Service
public class JpaTypeOfWineService implements TypeOfWineService {
	
	@Autowired
	TypeOfWineRepository typeRepository;
	
	@Override
	public List<TypeOfWine> getAll() {
		
		return typeRepository.getAll(); // return typeRepository.findAll();
	}

	@Override
	public TypeOfWine getOne(Long id) {
		
		return typeRepository.getOne(id);
	}

}
