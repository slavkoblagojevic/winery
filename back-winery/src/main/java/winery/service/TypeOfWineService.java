package winery.service;

import java.util.List;

import winery.model.TypeOfWine;

public interface TypeOfWineService {

		List<TypeOfWine> getAll();
		TypeOfWine getOne(Long id);
}
