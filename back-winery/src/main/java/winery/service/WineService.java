package winery.service;

import java.util.Optional;

import org.springframework.data.domain.Page;

import winery.model.Wine;

public interface WineService {
	
	Optional<Wine> getOne(Long id);

	Wine deleteById(Long id);

	Page<Wine> getAll(Long wineryId, String wineName, int pageNum);

	Wine create(Wine wine);

	Wine update(Wine wine);
}
