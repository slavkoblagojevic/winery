package winery.service;

import java.util.List;

import winery.model.Winery;

public interface WineryService {
	
	List<Winery> getAll();
	Winery getOne(Long id);
}
