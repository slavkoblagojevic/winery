package winery.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import winery.model.Winery;

@Repository
public interface WineryRepository extends JpaRepository<Winery, Long> {
	
}
