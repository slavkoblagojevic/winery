package winery.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import winery.model.Wine;

@Repository
public interface WineRepository extends JpaRepository<Wine, Long> {

	//SELECT * FROM winery.wine WHERE wine.id = 1
	
	@Query("SELECT wine FROM Wine wine WHERE wine.id LIKE :id")
	Optional<Wine> getWineById(@Param("id") Long id);

	Page<Wine> findAllByWineryIdAndWineNameContainingIgnoreCase(Long wineryId, String wineName, Pageable pageable);

	Page<Wine> findAllByWineNameContainingIgnoreCase(String wineName, Pageable pageable);
}
