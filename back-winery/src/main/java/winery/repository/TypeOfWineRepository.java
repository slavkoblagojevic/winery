package winery.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import winery.model.TypeOfWine;

@Repository
public interface TypeOfWineRepository extends JpaRepository<TypeOfWine, Long> {
	
	//SELECT * FROM winery.type_of_wine;
	
	@Query("SELECT type FROM TypeOfWine type")
	List<TypeOfWine> getAll();
}
