package winery.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import winery.model.Wine;
import winery.service.WineService;
import winery.support.WineDtoToWine;
import winery.support.WineToWineDto;
import winery.web.dto.WineDto;

@RestController
@RequestMapping(value = "/api/wines", produces = MediaType.APPLICATION_JSON_VALUE)
public class ApiWineController {

	@Autowired
	WineService wineService;

	@Autowired
	WineToWineDto toWineDto;
	
	@Autowired
	WineDtoToWine toWine;

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<WineDto> getOne(@PathVariable Long id) {

		Optional<Wine> wine = wineService.getOne(id);

		if (wine.isPresent()) {
			return new ResponseEntity<>(toWineDto.convert(wine.get()), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<WineDto> delete(@PathVariable Long id) {

		Wine wine = wineService.deleteById(id);

		if (wine == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(toWineDto.convert(wine), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@GetMapping
	public ResponseEntity<List<WineDto>> getAll(
			@RequestParam(required = false) Long wineryId, 
			@RequestParam(required = false) String wineName,
			@RequestParam(defaultValue = "0") int pageNum) {

		Page<Wine> page = wineService.getAll(wineryId, wineName, pageNum);
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Total-Pages", Integer.toString(page.getTotalPages()));
		
		
		return new ResponseEntity<>(toWineDto.convert(page.getContent()), headers ,HttpStatus.OK);
	}
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<WineDto> create(@Valid @RequestBody WineDto wineDto){
		
		Wine wine = toWine.convert(wineDto);
		
		Wine savedWine = wineService.create(wine);
		
		return new ResponseEntity<>(toWineDto.convert(savedWine), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasAnyRole('ROLE_KORISNIK', 'ROLE_ADMIN')")
	@PutMapping("/{id}")
	public ResponseEntity<WineDto> update(@PathVariable Long id,@Valid @RequestBody WineDto wineDto){
		
		if(!id.equals(wineDto.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Wine wine = wineService.update(toWine.convert(wineDto));
		
		return new ResponseEntity<>(toWineDto.convert(wine), HttpStatus.OK);
	}
}
