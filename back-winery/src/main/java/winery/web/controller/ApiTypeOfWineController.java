package winery.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import winery.model.TypeOfWine;
import winery.service.TypeOfWineService;
import winery.support.TypeOfWineToTypeOfWineDto;
import winery.web.dto.TypeOfWineDto;

@RestController
@RequestMapping(value = "/api/winetypes", produces = MediaType.APPLICATION_JSON_VALUE )
public class ApiTypeOfWineController {

	@Autowired
	TypeOfWineToTypeOfWineDto toTypesDto;
	
	@Autowired
	TypeOfWineService typeOfWineService;
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<TypeOfWineDto>> getAll() {

		List<TypeOfWine> typeOfWines = typeOfWineService.getAll();
		
		return new ResponseEntity<>(toTypesDto.convert(typeOfWines), HttpStatus.OK);
	}
	
	
}
