package winery.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class WineDto {
	
	//@Positive(message = "Id must be positive number.")
	private Long id;
	
	private String wineName;
	
	@NotBlank(message = "Wine name is not specified.")
	@Size(min=3, max=255)
	private String description;
	
	@Positive(message = "Production year must be positive number.")
	private int productionYear;
	
	@Positive(message = "Price must be positive number.")
	private double pricePerBottle;
	
	private int bottlesAvailable;
	
	private Long typeId;
	
	private String typeName;
	
	private Long wineryId;
	
	private String wineryName;

	public WineDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWineName() {
		return wineName;
	}

	public void setWineName(String wineName) {
		this.wineName = wineName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}

	public double getPricePerBottle() {
		return pricePerBottle;
	}

	public void setPricePerBottle(double pricePerBottle) {
		this.pricePerBottle = pricePerBottle;
	}

	public int getBottlesAvailable() {
		return bottlesAvailable;
	}

	public void setBottlesAvailable(int bottlesAvailable) {
		this.bottlesAvailable = bottlesAvailable;
	}

	public Long getTypeId() {
		return typeId;
	}

	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public Long getWineryId() {
		return wineryId;
	}

	public void setWineryId(Long wineryId) {
		this.wineryId = wineryId;
	}

	public String getWineryName() {
		return wineryName;
	}

	public void setWineryName(String wineryName) {
		this.wineryName = wineryName;
	}
	
	
}
