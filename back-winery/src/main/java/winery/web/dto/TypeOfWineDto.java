package winery.web.dto;

public class TypeOfWineDto {

	private Long id;
	
	private String typeName;

	public TypeOfWineDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
}
