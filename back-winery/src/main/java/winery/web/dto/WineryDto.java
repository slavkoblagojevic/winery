package winery.web.dto;

public class WineryDto {

	private Long id;
	
	private String wineryName;
	
	private int establishmentYear;

	public WineryDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWineryName() {
		return wineryName;
	}

	public void setWineryName(String wineryName) {
		this.wineryName = wineryName;
	}

	public int getEstablishmentYear() {
		return establishmentYear;
	}

	public void setEstablishmentYear(int establishmentYear) {
		this.establishmentYear = establishmentYear;
	}
	
	
}
