package winery.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import winery.model.Winery;
import winery.web.dto.WineryDto;

@Component
public class WineryToWineryDto implements Converter<Winery, WineryDto> {

	@Override
	public WineryDto convert(Winery winery) {
		
		WineryDto wineryDto = new WineryDto();
		
		wineryDto.setId(winery.getId());
		wineryDto.setWineryName(winery.getWineryName());
		wineryDto.setEstablishmentYear(winery.getEstablishmentYear());
		
		return wineryDto;
	}
	
	public List<WineryDto> convert(List<Winery> wineries){
		
		List<WineryDto> wineriesDto = new ArrayList<WineryDto>();
		
		for(Winery win : wineries) {
			wineriesDto.add(convert(win));
		}
		
		return wineriesDto;
	}

}
