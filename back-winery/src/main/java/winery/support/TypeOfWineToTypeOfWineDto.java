package winery.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import winery.model.TypeOfWine;
import winery.web.dto.TypeOfWineDto;

@Component
public class TypeOfWineToTypeOfWineDto implements Converter<TypeOfWine, TypeOfWineDto> {

	@Override
	public TypeOfWineDto convert(TypeOfWine type) {
		
		TypeOfWineDto typeDto = new TypeOfWineDto();
		
		typeDto.setId(type.getId());
		typeDto.setTypeName(type.getTypeName());
		
		return typeDto;
	}
	
	public List<TypeOfWineDto> convert(List<TypeOfWine> types){
		
		List<TypeOfWineDto> typesDto = new ArrayList<TypeOfWineDto>();
		
		for(TypeOfWine tp : types) {
			typesDto.add(convert(tp));
		}
		
		return typesDto;
	}

}
