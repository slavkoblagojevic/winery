package winery.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import winery.model.Wine;
import winery.service.TypeOfWineService;
import winery.service.WineryService;
import winery.web.dto.WineDto;

@Component
public class WineDtoToWine implements Converter<WineDto, Wine> {

	@Autowired
	TypeOfWineService typeOfWineService;
	
	@Autowired
	WineryService wineryService;
	
	@Override
	public Wine convert(WineDto wineDto) {
		
		Wine wine = new Wine();
		
		wine.setId(wineDto.getId());
		wine.setWineName(wineDto.getWineName());
		wine.setDescription(wineDto.getDescription());
		wine.setProductionYear(wineDto.getProductionYear());
		wine.setPricePerBottle(wineDto.getPricePerBottle());
		wine.setBottlesAvailable(wineDto.getBottlesAvailable());
		wine.setTypeOfWine(typeOfWineService.getOne(wineDto.getTypeId()));
		wine.setWinery(wineryService.getOne(wineDto.getWineryId()));
		
		return wine;
	}

}
