package winery.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import winery.model.Wine;
import winery.web.dto.WineDto;

@Component
public class WineToWineDto implements Converter<Wine, WineDto> {

	@Override
	public WineDto convert(Wine wine) {
		
		WineDto wineDto = new WineDto();
		
		wineDto.setId(wine.getId());
		wineDto.setWineName(wine.getWineName());
		wineDto.setDescription(wine.getDescription());
		wineDto.setProductionYear(wine.getProductionYear());
		wineDto.setPricePerBottle(wine.getPricePerBottle());
		wineDto.setBottlesAvailable(wine.getBottlesAvailable());
		wineDto.setTypeId(wine.getTypeOfWine().getId());
		wineDto.setTypeName(wine.getTypeOfWine().getTypeName());
		wineDto.setWineryId(wine.getWinery().getId());
		wineDto.setWineryName(wine.getWinery().getWineryName());
		
		return wineDto;
	}

	public List<WineDto> convert(List<Wine> wines){
		
		List<WineDto> winesDto = new ArrayList<WineDto>();
		
		for(Wine w : wines) {
			winesDto.add(convert(w));
		}
		
		return winesDto;
	}
}
