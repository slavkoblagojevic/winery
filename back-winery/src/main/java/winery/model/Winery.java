package winery.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Winery {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String wineryName;
	
	@Column
	private int establishmentYear;
	
	@OneToMany( mappedBy = "winery", cascade = CascadeType.ALL)
	private List<Wine> wines = new ArrayList<Wine>();

	public Winery() {
		super();
	}

	public Winery(Long id, String wineryName, int establishmentYear, List<Wine> wines) {
		super();
		this.id = id;
		this.wineryName = wineryName;
		this.establishmentYear = establishmentYear;
		this.wines = wines;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWineryName() {
		return wineryName;
	}

	public void setWineryName(String wineryName) {
		this.wineryName = wineryName;
	}

	public int getEstablishmentYear() {
		return establishmentYear;
	}

	public void setEstablishmentYear(int establishmentYear) {
		this.establishmentYear = establishmentYear;
	}

	public List<Wine> getWines() {
		return wines;
	}

	public void setWines(List<Wine> wines) {
		this.wines = wines;
	}

	@Override
	public String toString() {
		return "Winery [id=" + id + ", wineryName=" + wineryName + ", establishmentYear=" + establishmentYear
				+ ", wines=" + wines + "]";
	}
	
}
