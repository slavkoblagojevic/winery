package winery.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Wine {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String wineName;
	
	@Column
	private String description;
	
	@Column(nullable = false)
	private int productionYear;
	
	@Column
	private double pricePerBottle;
	
	@Column
	private int bottlesAvailable;
	
	@ManyToOne
	private TypeOfWine typeOfWine;
	
	@ManyToOne
	private Winery winery;

	public Wine() {
		super();
	}

	public Wine(Long id, String wineName, String description, int productionYear, double pricePerBottle,
			int bottlesAvailable, TypeOfWine typeOfWine, Winery winery) {
		super();
		this.id = id;
		this.wineName = wineName;
		this.description = description;
		this.productionYear = productionYear;
		this.pricePerBottle = pricePerBottle;
		this.bottlesAvailable = bottlesAvailable;
		this.typeOfWine = typeOfWine;
		this.winery = winery;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWineName() {
		return wineName;
	}

	public void setWineName(String wineName) {
		this.wineName = wineName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}

	public double getPricePerBottle() {
		return pricePerBottle;
	}

	public void setPricePerBottle(double pricePerBottle) {
		this.pricePerBottle = pricePerBottle;
	}

	public int getBottlesAvailable() {
		return bottlesAvailable;
	}

	public void setBottlesAvailable(int bottlesAvailable) {
		this.bottlesAvailable = bottlesAvailable;
	}

	public TypeOfWine getTypeOfWine() {
		return typeOfWine;
	}

	public void setTypeOfWine(TypeOfWine typeOfWine) {
		this.typeOfWine = typeOfWine;
	}

	public Winery getWinery() {
		return winery;
	}

	public void setWinery(Winery winery) {
		this.winery = winery;
	}

	@Override
	public String toString() {
		return "Wine [id=" + id + ", wineName=" + wineName + ", description=" + description + ", productionYear="
				+ productionYear + ", pricePerBottle=" + pricePerBottle + ", bottlesAvailable=" + bottlesAvailable
				+ ", typeOfWine=" + typeOfWine + ", winery=" + winery + "]";
	}
	
	
	
	
}
