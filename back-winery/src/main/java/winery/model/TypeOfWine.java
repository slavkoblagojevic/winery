package winery.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TypeOfWine {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private String typeName;
	
	@OneToMany(mappedBy = "typeOfWine", cascade = CascadeType.ALL)
	private List<Wine> wines = new ArrayList<Wine>();

	public TypeOfWine() {
		super();
	}

	public TypeOfWine(Long id, String typeName, List<Wine> wines) {
		super();
		this.id = id;
		this.typeName = typeName;
		this.wines = wines;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public List<Wine> getWines() {
		return wines;
	}

	public void setWines(List<Wine> wines) {
		this.wines = wines;
	}

	@Override
	public String toString() {
		return "TypeOfWine [id=" + id + ", typeName=" + typeName + ", wines=" + wines + "]";
	}
	
	
}
