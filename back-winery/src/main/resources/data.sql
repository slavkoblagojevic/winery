INSERT INTO `user` (id, username, password, role)
              VALUES (1,'miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','ADMIN');
INSERT INTO `user` (id, username, password, role)
              VALUES (2,'tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','KORISNIK');

INSERT INTO `user` (id, username, password, role)
              VALUES (3,'petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','KORISNIK');
            
INSERT INTO `user` (id, username, password, role)
              VALUES (4,'srdjan','$2a$12$fxagy0tjs2bOmCy/.3nwL.qSW8TvqWbaVG6fiPCRm.ljaVovqobvm','KORISNIK');

INSERT INTO `user` (id, username, password, role)
              VALUES (5,'srdjan1','$2a$12$ojxhd54XXknRkLV3esAMV.B/ADghP3nYWJF.4YXVAwISsapGTbgcy','ADMIN');
              
INSERT INTO type_of_wine (id, type_name) VALUES (1, 'Red');       
INSERT INTO type_of_wine (id, type_name) VALUES (2, 'White');    
INSERT INTO type_of_wine (id, type_name) VALUES (3, 'Rosé'); 
INSERT INTO type_of_wine (id, type_name) VALUES (4, 'Sparkling');    
INSERT INTO type_of_wine (id, type_name) VALUES (5, 'Dessert');    
INSERT INTO type_of_wine (id, type_name) VALUES (6, 'Fortified');

INSERT INTO winery (id, winery_name, establishment_year) VALUES (1, 'Kiš', 1830);
INSERT INTO winery (id, winery_name, establishment_year) VALUES (2, 'Kovačević', 2001);
INSERT INTO winery (id, winery_name, establishment_year) VALUES (3, 'Erdevik', 1826);
INSERT INTO winery (id, winery_name, establishment_year) VALUES (4, 'Zvonko Bogdan', 2008);
INSERT INTO winery (id, winery_name, establishment_year) VALUES (5, 'Čoka', 1903);


INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (1, '8 Guitar Players', 'A light white wine with aromas of summer fruit.',
		2010, 21, 6000, 2, 4);
INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (2, 'Life Flows', 'Aromas of ripe black cherry, plum and blackberry.',
		2008, 36, 4000, 1, 4);
INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (3, '4 Fat Horses', 'The charming notes of freshly-picked strawberries, raspberries 
			and red currants.',
		2011, 45, 8000, 3, 4);
INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (4, 'Portugizer', 'The wine is characterized by a dense, bluish-purple color.',
		2020, 10, 5000, 1, 1);
INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (5, 'Orfeline', 'Moderate sweetness, drinkable and light wine.',
		2015, 4, 3000, 3, 2);
INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (6, 'Bella Novella', 'The intense aroma of the wine is dominated by the aromas of peach and pear.',
		2018, 8, 2500, 2, 3);
INSERT INTO wine (id, wine_name, description, production_year, price_per_bottle, bottles_available, type_of_wine_id, winery_id) 
VALUES (7, 'Geronimo', 'Geronimo is a young, potent wine with fragrant notes of lime.',
		2019, 15, 1850, 2, 3);
		
