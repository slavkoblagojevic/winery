## Winery

## Technologies: SpringBoot, REST API, ReactJS (functional components), Bootstrap

The application provides work with entities: Wine, Type Of Wine and Winery.

Using the Spring Boot framework, the following REST APIs are implemented:

GET /api/winetypes – endpoint to fetch all types of wine admin, user;
GET /api/wineries - endpoint to fetch all wineries admin, user;
GET /api/wines - endpoint to fetch all wines (pagination) admin, user;
GET /api/wines/{id} - endpoint to fetch wine by id admin, user;
POST /api/wines - endpoint to create new wine admin;
PUT /api/wines/{id} - endpoint to update existing one admin;
DELETE /api/wines/{id} – endpoint to delete wine admin;

The application provides the following functionality: 
Entry of a new wine on a separate page, delete wine, search based on several criteria, paginated display of data,
admin has the option of ordering wine inside the winery 
if the registered user is not an administrator but a regular user, he can buy wine.
